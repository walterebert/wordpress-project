Starter project for WordPress
=============================

After a standard WordPress installation, you have a setup that only has the most
basic settings configured. There is a lot of settings you can tweak to improve
security, performance and/or usability.

Here you can find an example project setup for common use cases.

The configuration can be used to install and update a WordPress site using
[Composer](https://getcomposer.org/). The setup already includes some useful
plugins. Change the `composer.json` file to fit your needs. Available plugins
for Composer can be found on [WordPress Packagist](http://wpackagist.org/). All
stable releases from [wordpress.org](https://wordpress.org/) should be
available.

A database **must** already be configured. The install procedure will not create
a database for you.

To install WordPress, run:
```
composer create-project wee/wordpress-project --no-dev
```

To install WordPress with development plugins, run:
```
composer create-project wee/wordpress-project
```

If you did a git checkout, you can install WordPress with:
```
composer install --no-dev
```

To install WordPress with development plugins, run:
```
composer install
```

Composer will start to install and configure WordPress. It will ask for your database credentials
and information for your site. After the install you should have a .env file that contains your
database and WordPress settings.

To log into WordPress go to http://localhost/wp/wp-login.php (or the domain you defined).

After Composer has finished, WordPress will be available in the `web` folder.
You can change the path in `composer.json`. After the install you can remove
the `src/web` folder.

If you do not plan to update WordPress with Composer, you can remove the
`composer.json` and `composer.lock` files and `src` folder after the install.

See `composer-full.json` for a large selection of packages that could be useful.

If you intend to update WordPress with Composer only, add the following lines to the `.env` file.
This will disable auto-updates and updates from the WordPress admin.

```
WP_AUTO_UPDATE_CORE=0
WP_AUTOMATIC_UPDATER_DISABLED=1
WP_DISALLOW_FILE_MODS=1
```

See `.env-reference` for available settings / examples.

To update WordPress, including plugins and themes, run:
```
composer update --no-dev
```

Or with development plugins:
```
composer update
```

You can also use [WP-CLI](http://wp-cli.org/) to update WordPress:
```
vendor/bin/wp core update
vendor/bin/wp core update-db
vendor/bin/wp language core update
vendor/bin/wp plugin update --all
vendor/bin/wp language plugin update --all
vendor/bin/wp theme update --all
vendor/bin/wp language theme update --all
```

Author
------
[Walter Ebert](https://wee.press/)

Reference
---------
* [Composer](https://getcomposer.org/)
* [WordPress Packagist](https://wpackagist.org/)
* [Improving WordPress Development Workflow with Composer](https://torquemag.io/2014/09/improving-wordpress-development-workflow-composer/)
* [Your Guide to Composer in WordPress](https://composer.rarst.net/)
* [Apache documentation](https://httpd.apache.org/docs/2.4/)
* [WordPress Codex on .htaccess](https://codex.wordpress.org/htaccess)
* [HTML5 boilerplate Apache configuration](https://github.com/h5bp/server-configs-apache)
* [Ask Apache on .htaccess](https://www.askapache.com/htaccess/)
* [Editing wp-config.php](https://wordpress.org/support/article/editing-wp-config-php/)
