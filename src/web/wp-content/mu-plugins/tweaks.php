<?php
/**
 * Plugin Name: Must Use tweaks
 * Description: Adds custom tweaks to WordPress.
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	http_response_code( 403 );
	die( 'Forbidden' );
}

/* Clean up HTML <head> */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'start_post_rel_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link' );

/* Clean up WordPress backend */
if ( is_admin() ) {
	add_action(
		'admin_menu',
		function() {
			/* Hide admin menu items */
			// remove_menu_page( 'index.php' ); // Dashboard.
			// remove_menu_page( 'edit.php' ); // Posts.
			// remove_menu_page( 'upload.php' ); // Media.
			// remove_menu_page( 'edit.php?post_type=page' ); // Pages.
			// remove_menu_page( 'edit-comments.php' ); // Comments.
			// remove_menu_page( 'themes.php' ); // Appearance.
			// remove_menu_page( 'plugins.php' ); // Plugins.
			// remove_menu_page( 'users.php' ); // Users.
			// remove_menu_page( 'tools.php' ); // Tools.
			// remove_menu_page( 'options-general.php' ); // Settings.
			// remove_menu_page( 'edit.php?post_type=custom_post_type' ); // Custom post type.

			/* Remove news from dashboard */
			remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
		}
	);

	/* Remove welcome panel from dashboard */
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
}

/* Clean up admin bar */
add_action(
	'wp_before_admin_bar_render',
	function() {
		global $wp_admin_bar;

		// $wp_admin_bar->remove_menu( 'wp-logo' );
		// $wp_admin_bar->remove_menu( 'about' );
		// $wp_admin_bar->remove_menu( 'wporg' );
		// $wp_admin_bar->remove_menu( 'documentation' );
		// $wp_admin_bar->remove_menu( 'support-forums' );
		// $wp_admin_bar->remove_menu( 'feedback' );
		// $wp_admin_bar->remove_menu( 'site-name' );
		// $wp_admin_bar->remove_menu( 'view-site' );
		// $wp_admin_bar->remove_menu( 'updates' );
		// $wp_admin_bar->remove_menu( 'comments' );
		// $wp_admin_bar->remove_menu( 'new-content' );
		// $wp_admin_bar->remove_menu( 'new-content-default' );
		// $wp_admin_bar->remove_menu( 'new-post' );
		// $wp_admin_bar->remove_menu( 'new-media' );
		// $wp_admin_bar->remove_menu( 'new-page' );
		// $wp_admin_bar->remove_menu( 'new-user' );
		// $wp_admin_bar->remove_menu( 'my-account' );
		// $wp_admin_bar->remove_menu( 'user-actions' );
		// $wp_admin_bar->remove_menu( 'user-info' );
		// $wp_admin_bar->remove_menu( 'edit-profile' );
		// $wp_admin_bar->remove_menu( 'logout' );
	}
);

/* Add filters */
add_action(
	'plugins_loaded',
	function() {
		/* Use generic GUIDs */
		add_filter(
			'wp_insert_post_data',
			function( $data, $postarr ) {
				if ( '' === $data['guid'] && class_exists( 'Ulid\Ulid' ) ) {
					/* Use ULID https://github.com/ulid/spec */
					$data['guid'] = 'urn:ulid:' . Ulid\Ulid::generate();
					if ( ! empty( $data['post_date'] ) ) {
						$data['guid'] = 'urn:ulid:' . Ulid\Ulid::fromTimestamp( strtotime( $data['post_date'] ) );
					}
				} elseif ( '' === $data['guid'] ) {
					/* Use UUID */
					$data['guid'] = 'urn:uuid:' . wp_generate_uuid4();
				}

				return $data;
			},
			11,
			2
		);

		/* Add UUID/ULID to the post class */
		add_filter(
			'post_class',
			function ( $classes ) {
				$guid = get_the_guid();
				if (
					strpos( $guid, 'urn:ulid:' ) === 0 ||
					strpos( $guid, 'urn:uuid:' ) === 0
				) {
					$guid = str_replace(
						array( 'urn:ulid:', 'urn:uuid:' ),
						'',
						$guid
					);
					$classes[] = esc_attr( $guid );
				}
				return $classes;
			}
		);

		/* Disable writes to .htaccess file */
		add_filter( 'flush_rewrite_rules_hard', '__return_false' );

		/* Do not log IP address for comments */
		add_filter( 'pre_comment_user_ip', '__return_empty_string' );

		/* Hide pingback URL */
		add_filter(
			'wp',
			function() {
				header_remove( 'X-Pingback' );
			}
		);

		/* Remove default emoji resource hints */
		add_filter(
			'wp_resource_hints',
			function( $hints, $relation_type ) {
				foreach ( $hints as $k => $v ) {
					if ( strpos( $v, '//s.w.org/' ) ) {
						unset( $hints[ $k ] );
					}
				}
				return $hints;
			},
			10,
			2
		);

		/*
		if ( defined( 'WP_AUTO_UPDATE_CORE' ) && WP_AUTO_UPDATE_CORE && WP_AUTO_UPDATE_CORE !== 'minor' ) {
			// Enable automatic updates for all plugins.
			add_filter( 'auto_update_plugin', '__return_true' );

			// Enable automatic updates for all themes.
			add_filter( 'auto_update_theme', '__return_true' );
		}
		*/

		/* Re-activate the WordPress Links Manager */
		// add_filter( 'pre_option_link_manager_enabled', '__return_true' );

		/*
		Ignore region subtags
		https://www.w3.org/International/articles/language-tags/index.en#region
		*/
		add_filter(
			'language_attributes',
			function( $output, $doctype ) {
				return str_replace(
					array( 'en-US', 'en-GB', 'de-DE' ),
					array( 'en', 'en', 'de' ),
					$output
				);
			},
			10,
			2
		);

		/*
		- Use YouTube privacy-enhanced mode https://support.google.com/youtube/answer/171780?visit_id=637419781721286051-946666784&rd=1
		- Add lazy loading
		*/
		add_filter(
			'oembed_result',
			function( $data, $url, $args ) {
				if ( preg_match('#https?://(www\.)?youtu\.?be(\.com)?/#i', $url) ) {
					return str_replace(
						' src="https://www.youtube.com/embed/',
						' loading="lazy" src="https://www.youtube-nocookie.com/embed/',
						$data
					);
				}

				return $data;
			},
			11,
			3
		);
	},
	10,
	0
);
