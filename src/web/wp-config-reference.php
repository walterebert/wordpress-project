<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/* Composer Autoloader */
require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

/* Load .env file */
$dotenv = Dotenv\Dotenv::createImmutable(  __DIR__ . DIRECTORY_SEPARATOR . '..' );
$dotenv->load();

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define(
	'DB_NAME',
	empty($_ENV['WP_DB_NAME']) ? 'localhost' : $_ENV['WP_DB_NAME']
);

/** MySQL database username */
define(
	'DB_USER',
	empty($_ENV['WP_DB_USER']) ? 'localhost' : $_ENV['WP_DB_USER']
);

/** MySQL database password */
define(
	'DB_PASSWORD',
	empty($_ENV['WP_DB_PASSWORD']) ? 'localhost' : $_ENV['WP_DB_PASSWORD']
);

/** MySQL hostname */
define(
	'DB_HOST',
	empty( $_ENV['WP_DB_HOST'] ) ? '127.0.0.1' : $_ENV['WP_DB_HOST']
);

/** Database Charset to use in creating database tables. */
define(
	'DB_CHARSET',
	empty( $_ENV['WP_DB_CHARSET'] ) ? 'utf8mb4' : $_ENV['WP_DB_CHARSET']
);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         $_ENV['WP_AUTH_KEY'] );
define( 'SECURE_AUTH_KEY',  $_ENV['WP_SECURE_AUTH_KEY'] );
define( 'LOGGED_IN_KEY',    $_ENV['WP_LOGGED_IN_KEY'] );
define( 'NONCE_KEY',        $_ENV['WP_NONCE_KEY'] );
define( 'AUTH_SALT',        $_ENV['WP_AUTH_SALT'] );
define( 'SECURE_AUTH_SALT', $_ENV['WP_SECURE_AUTH_SALT'] );
define( 'LOGGED_IN_SALT',   $_ENV['WP_LOGGED_IN_SALT'] );
define( 'NONCE_SALT',       $_ENV['WP_NONCE_SALT'] );


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = empty( $_ENV['WP_DB_PREFIX'] ) ? 'wp_' : $_ENV['WP_DB_PREFIX'];

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define(
	'WP_DEBUG',
	empty( $_ENV['WP_DEBUG'] ) ? false : (bool) $_ENV['WP_DEBUG']
);

/* Use HTTPS for login / admin */
define(
	'FORCE_SSL_LOGIN',
	empty( $_ENV['WP_FORCE_SSL_LOGIN'] ) ? false : (bool) $_ENV['WP_FORCE_SSL_LOGIN']
);
define(
	'FORCE_SSL_ADMIN',
	empty( $_ENV['WP_FORCE_SSL_ADMIN'] ) ? false : (bool) $_ENV['WP_FORCE_SSL_ADMIN']
);

/* HTTPS for reverse proxies https://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && stripos( 0 === $_SERVER['HTTP_X_FORWARDED_PROTO'], 'https' )  ) {
	$_SERVER['HTTPS'] = 'on';
}

/* HTTP_HOST for reverse proxies https://wordpress.org/support/topic/wordpress-behind-reverse-proxy-1/ */
if ( ! empty( $_SERVER['HTTP_X_FORWARDED_HOST'] ) ) {
	$_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
}

/* Custom site URLs. Warning: FILTER_SANITIZE_URL is not compatible with non-ASCII domains */
(function() {
	$scheme = 'http';
	if ( ! empty( $_SERVER['HTTPS'] ) ) {
		$scheme = 'https';
	}
	$base_url = $scheme . '://' . filter_input( INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL );
	if ( ! empty( $_ENV['WP_BASE_URL'] ) ) {
		$base_url = $_ENV['WP_BASE_URL'];
	}
	define( 'WP_SITEURL', $base_url . '/wp' );
	define( 'WP_HOME', $base_url );

	/* Custom content directory */
	define( 'WP_CONTENT_URL', $base_url . '/wp-content' );
	define( 'WP_CONTENT_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'wp-content' );
})();

/* Expose caching functionality */
define(
	'WP_CACHE',
	empty( $_ENV[ 'WP_CACHE'] ) ? false : (bool) $_ENV[ 'WP_CACHE']
);

/* Clean up database */
define(
	'WP_POST_REVISIONS',
	$_ENV['WP_POST_REVISIONS'] ?: 5
);
define(
	'MEDIA_TRASH',
	empty( $_ENV['WP_MEDIA_TRASH'] ) ? false : (bool) $_ENV['WP_MEDIA_TRASH']
);
define(
	'EMPTY_TRASH_DAYS',
	empty( $_ENV['WP_EMPTY_TRASH_DAYS'] ) ? 14 : (int) $_ENV['WP_EMPTY_TRASH_DAYS']
);

/* Increase memory for WooCommerce */
define(
	'WP_MEMORY_LIMIT',
	empty( $_ENV['WP_MEMORY_LIMIT'] ) ? '64M' : $_ENV['WP_MEMORY_LIMIT']
);

/**
 * Admin settings that will work with multidomains out-of-the-box
 * http://kaspars.net/blog/wordpress/wordpress-multisite-without-domain-mapping
 * http://tommcfarlin.com/resolving-the-wordpress-multisite-redirect-loop/
 */
/*
define( 'ADMIN_COOKIE_PATH', '/' );
define( 'COOKIE_DOMAIN', '' );
define( 'COOKIEPATH', '' );
define( 'SITECOOKIEPATH', '' );
*/

/* Multisite */
define( 'WP_ALLOW_MULTISITE', false );

/* Updates https://codex.wordpress.org/Configuring_Automatic_Background_Updates */
(function() {
	$WP_AUTO_UPDATE_CORE = isset( $_ENV['WP_AUTO_UPDATE_CORE'] ) ? $_ENV['WP_AUTO_UPDATE_CORE'] : 'minor';
	if ( $WP_AUTO_UPDATE_CORE !== 'minor' ) {
		$WP_AUTO_UPDATE_CORE = (bool) $WP_AUTO_UPDATE_CORE;
	}
	define( 'WP_AUTO_UPDATE_CORE', $WP_AUTO_UPDATE_CORE );
})();
define(
	'AUTOMATIC_UPDATER_DISABLED',
	empty( $_ENV['WP_AUTOMATIC_UPDATER_DISABLED'] ) ? false : (bool) $_ENV['WP_AUTOMATIC_UPDATER_DISABLED']
);

/* Security */
define(
	'DISALLOW_FILE_MODS',
	empty( $_ENV['WP_DISALLOW_FILE_MODS'] ) ? false : (bool) $_ENV['WP_DISALLOW_FILE_MODS']
);
define(
	'DISALLOW_FILE_EDIT',
	empty( $_ENV['WP_DISALLOW_FILE_EDIT'] ) ? false : (bool) $_ENV['WP_DISALLOW_FILE_EDIT']
);
define(
	'WP_HTTP_BLOCK_EXTERNAL',
	empty( $_ENV['WP_HTTP_BLOCK_EXTERNAL'] ) ? false : (bool) $_ENV[ 'WP_HTTP_BLOCK_EXTERNAL']
);
define(
	'WP_ACCESSIBLE_HOSTS',
	empty( $_ENV['WP_ACCESSIBLE_HOSTS'] ) ? filter_input( INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_URL ) . ',*.wordpress.org,wordpress.org' : $_ENV['WP_ACCESSIBLE_HOSTS']
);

/* Cronjob */
if ( ! empty( $_ENV['DISABLE_WP_CRON'] ) ) {
	define( 'DISABLE_WP_CRON', true );
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
