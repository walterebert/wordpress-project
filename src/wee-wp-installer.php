<?php
/**
 * Composer installer
 *
 * @package wee/wordpress-project
 */

use Composer\Script\Event;

/**
 * Installer class
 */
class Wee_WP_Installer
{
	const WEB_DIR = 'web';
	const WP_DIR = 'wp';
	const CONTENT_DIR = 'wp-content';
	const DIR_PERMS = 0744;

	/**
	 * Generate a random string
	 *
	 * @param  integer $length String length.
	 * @param  boolean $specialchars Include special characters.
	 * @return string
	 */
	protected static function random_string( $length = 64, $specialchars = true ) {
		$chars = 'abcdefhijkmnpqrstuvwxyz2345678ABCDEFGHJKLMNPQRSTUVWXYZ2345678';
		if ( $specialchars ) {
			$chars .= '!@#$%^&*()-_[]{}<>~+=,.;:/?';
		}
		$max = strlen( $chars ) - 1;

		$string = '';
		for ( $i = 0; $i < $length; $i++ ) {
			$string .= substr(
				$chars,
				random_int( 0 , $max ),
				1
			);
		}

		return $string;
	}

	/**
	 * Escape string for .env file
	 *
	 * @param string $string Input string.
	 * @return string
	 */
	protected static function escape( $string ) {
		$string = str_replace( '\\', '\\\\', (string) $string );
		return str_replace( '"', '\"', $string );
	}

	/**
	 * Create a new WordPress configuration
	 *
	 * @param  Event $event Composer Script Event object.
	 * @throws \Exception If directory or file is not writable.
	 * @throws \UnexpectedValueException If input is invalid.
	 * @return boolean
	 */
	public static function create_config( Event $event ) {
		$base_dir = realpath( __DIR__ . '/..' );
		if ( ! is_writable( $base_dir ) ) {
			throw new \Exception( 'Directory ' . (string) $base_dir . ' is not writable' );
		}

		// Check if environment configuration file already exists.
		$env = $base_dir . DIRECTORY_SEPARATOR . '.env';
		if ( file_exists( $env ) ) {
			echo 'Configuration file .env already exists' . PHP_EOL;
			return true;
		}

		// Copy sources.
		$src_root = $base_dir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . self::WEB_DIR;
		$web_root = $base_dir . DIRECTORY_SEPARATOR . self::WEB_DIR;

		$files = new \RecursiveIteratorIterator( new RecursiveDirectoryIterator( $src_root ) );
		foreach ( $files as $file ) {
			if ( $file->isFile() ) {
				$copy_file = str_replace( $src_root, $web_root, $file->getPathname() );

				$dir_name = dirname( $copy_file );
				if ( ! file_exists( $dir_name ) ) {
					mkdir( $dir_name, self::DIR_PERMS, true );
				}

				copy(
					$file->getPathname(),
					$copy_file
				);
			}
		}

		// Set default values in case --no-interaction flag is used.
		$admin_password = self::random_string( 16, false );
		$values = array(
			'DB_HOST' => 'localhost',
			'DB_NAME' => 'wordpress',
			'DB_USER' => 'root',
			'DB_PASSWORD' => '',
			'DB_PREFIX' => 'wp_',
			'DB_CHARSET' => 'utf8mb4',
			'DB_COLLATE' => 'utf8mb4_unicode_ci',
			'BASE_URL' => 'http://localhost',
			'TITLE' => '',
			'ADMIN_USER' => 'admin-install',
			'ADMIN_PASSWORD' => $admin_password,
			'ADMIN_EMAIL' => 'root@localhost',
		);
		$env_data = '';

		$config = $event->getComposer()->getConfig();
		$io = $event->getIO();
		if ( ! $io ) {
			return;
		}

		// Database settings.
		$values['DB_HOST'] = $io->ask(
			'Please provide the database host name (default: localhost): ',
			''
		);
		$values['DB_HOST'] = filter_var( $values['DB_HOST'], FILTER_SANITIZE_URL );
		if ( ! $values['DB_HOST'] ) {
			$values['DB_HOST'] = 'localhost';
		}
		$env_data .= 'WP_DB_HOST="' . self::escape( $values['DB_HOST'] )  . '"' . PHP_EOL;

		$values['DB_NAME'] = $io->askAndValidate(
			'Please provide the database name: ',
			function( $value ) {
				if ( preg_match( '/[^0-9a-z_\-]/iu', $value ) ) {
					throw new \UnexpectedValueException( 'Invalid database name' );
				}

				return $value;
			},
			5,
			'wordpress'
		);
		$env_data .= 'WP_DB_NAME="' . self::escape( $values['DB_NAME'] )  . '"' . PHP_EOL;

		$values['DB_USER'] = $io->askAndValidate(
			'Please provide the database username: ',
			function( $value ) {
				if ( preg_match( '/[^0-9a-z_\-]/iu', $value ) ) {
					throw new \UnexpectedValueException( 'Invalid database username' );
				}

				return $value;
			},
			5,
			'wordpress'
		);
		$env_data .= 'WP_DB_USER="' . self::escape( $values['DB_USER'] ) . '"' . PHP_EOL;

		$values['DB_PASSWORD'] = $io->askAndValidate(
			'Please provide the database password: ',
			function( $value ) use ( $io ) {
				if ( '' === trim( $value )  ) {
					if ( $io->askConfirmation( 'Use an empty password?', false ) ) {
						return '';
					}
				}

				return $value;
			},
			5,
			''
		);
		$env_data .= 'WP_DB_PASSWORD="' . self::escape( $values['DB_PASSWORD'] ) . '"' . PHP_EOL;

		$values['DB_CHARSET'] = $io->ask(
			'Please provide the database character set (default: utf8mb4): ',
			'utf8mb4'
		);
		$env_data .= 'WP_DB_CHARSET="' . self::escape( $values['DB_CHARSET'] ) . '"' . PHP_EOL;

		$values['DB_COLLATE'] = $io->ask(
			'Please provide the database character set collation (default: utf8mb4_unicode_ci): ',
			'utf8mb4_unicode_ci'
		);
		$env_data .= 'WP_DB_COLLATE="' . self::escape( $values['DB_COLLATE'] ) . '"' . PHP_EOL;

		$values['DB_PREFIX'] = $io->ask(
			'Please provide a database prefix (default: wp_): ',
			'wp_'
		);
		$values['DB_PREFIX'] = preg_replace( '/[^0-9a-z_]/ium', '', $values['DB_PREFIX'] );
		if ( ! $values['DB_PREFIX'] ) {
			$values['DB_PREFIX'] = 'wp_';
		}
		if ( '_' !== substr( $values['DB_PREFIX'], - 1, 1 ) ) {
			$values['DB_PREFIX'] = $values['DB_PREFIX'] . '_';
		}
		$env_data .= 'WP_DB_PREFIX="' . self::escape( $values['DB_PREFIX'] ) . '"' . PHP_EOL;

		$values['BASE_URL'] = $io->askAndValidate(
			'Please provide the URL to the WordPress site (e.g. http://example.com): ',
			function( $value ) {
				if ( stripos( $value, 'http://' ) === false && stripos( $value, 'https://' ) === false ) {
					$value = 'http://' . $value;
				}
				if ( ! filter_var( $value, FILTER_VALIDATE_URL ) ) {
					throw new \UnexpectedValueException( 'Invalid URL' );
				}
				return trim( $value, '/' );
			},
			5,
			'http://localhost'
		);
		$env_data .= 'WP_BASE_URL="' . self::escape( $values['BASE_URL'] ) . '"' . PHP_EOL;

		// Set salts.
		$env_data .= 'WP_AUTH_KEY="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_SECURE_AUTH_KEY="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_LOGGED_IN_KEY="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_NONCE_KEY="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_AUTH_SALT="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_SECURE_AUTH_SALT="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_LOGGED_IN_SALT="' . self::random_string() . '"' . PHP_EOL;
		$env_data .= 'WP_NONCE_SALT="' . self::random_string() . '"' . PHP_EOL;

		// Misc. settings.
		$env_data .= 'WP_DEBUG=1' . PHP_EOL;
		$env_data .= 'WP_CACHE=0' . PHP_EOL;
		$env_data .= 'WP_FORCE_SSL_LOGIN=0' . PHP_EOL;
		$env_data .= 'WP_FORCE_SSL_ADMIN=0' . PHP_EOL;
		$env_data .= 'WP_POST_REVISIONS=""' . PHP_EOL;
		$env_data .= 'WP_MEDIA_TRASH=5' . PHP_EOL;
		$env_data .= 'WP_EMPTY_TRASH_DAYS=14' . PHP_EOL;
		$env_data .= 'WP_MEMORY_LIMIT="64M"' . PHP_EOL;
		$env_data .= 'WP_AUTO_UPDATE_CORE="minor"' . PHP_EOL;
		$env_data .= 'WP_AUTOMATIC_UPDATER_DISABLED=0' . PHP_EOL;
		$env_data .= 'WP_DISALLOW_FILE_MODS=0' . PHP_EOL;
		$env_data .= 'WP_DISALLOW_FILE_EDIT=1' . PHP_EOL;
		$env_data .= 'WP_HTTP_BLOCK_EXTERNAL=0' . PHP_EOL;
		$env_data .= 'WP_ACCESSIBLE_HOSTS="localhost,*.wordpress.org,wordpress.org,*.wp.org,wp.org,*.planetwp.org"' . PHP_EOL;
		$env_data .= 'DISABLE_WP_CRON=0' . PHP_EOL;

		// Site settings.
		$values['TITLE'] = $io->ask(
			'Please provide the site title: ',
			''
		);

		$values['ADMIN_USER'] = $io->askAndValidate(
			'Please provide a WordPress username (e.g. admin-install): ',
			function( $value ) {
				if ( preg_match( '/[^0-9a-z_\-\.]/ium', $value ) || strlen( $value ) < 3 ) {
					throw new \UnexpectedValueException( 'Invalid username. Allowed characters: 0-9 a-z _ - .' );
				}

				return $value;
			},
			5,
			'admin-install'
		);

		$values['ADMIN_PASSWORD'] = $io->askAndValidate(
			'Please provide a user password (randomly generated: ' . (string) $admin_password . '): ',
			function( $value ) use ( $admin_password ) {
				if ( '' === trim( $value ) && $io->askConfirmation( 'Use ' . $admin_password . ' ?', false ) ) {
					return $admin_password;
				}

				return $value;
			},
			5,
			$admin_password
		);

		$admin_email = 'root@localhost.localdomain';
		$values['ADMIN_EMAIL'] = $io->askAndValidate(
			'Please provide a user email address (default: ' . $admin_email . '): ',
			function( $value ) use ($admin_email) {
				$value === trim( $value );
				if ( '' === $value ) {
					return $admin_email;
				} elseif ( ! filter_var( $value, FILTER_VALIDATE_EMAIL ) ) {
					throw new \UnexpectedValueException( 'Invalid email address' );
				}
				return $value;
			},
			1,
			$admin_email
		);

		$wp_base_dir = $base_dir . DIRECTORY_SEPARATOR . self::WEB_DIR . DIRECTORY_SEPARATOR . 'wp';

		// Write configuration.
		if ( ! file_put_contents( $env, $env_data ) ) {
			throw new \Exception( 'Could not write configuration to ' . (string) $env );
			return false;
		}

		// Write wp-config.php.
		$wp_core = $base_dir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR .  'bin' . DIRECTORY_SEPARATOR . 'wp core ';
		$wp_config = $wp_core . 'config ';
		$wp_config .= sprintf(
			'--dbhost=%s --dbname=%s --dbuser=%s --dbpass=%s --dbprefix=%s --dbcharset=%s --dbcollate=%s --path=%s',
			escapeshellarg( $values['DB_HOST'] ),
			escapeshellarg( $values['DB_NAME'] ),
			escapeshellarg( $values['DB_USER'] ),
			escapeshellarg( $values['DB_PASSWORD'] ),
			escapeshellarg( $values['DB_PREFIX'] ),
			escapeshellarg( $values['DB_CHARSET'] ),
			escapeshellarg( $values['DB_COLLATE'] ),
			escapeshellarg( $wp_base_dir )
		);

		echo exec( $wp_config ) . PHP_EOL;

		// Create database tables.
		$wp_install = $wp_core . 'install ';
		$wp_install .= sprintf(
			'--url=%s --title=%s --admin_user=%s --admin_password=%s --admin_email=%s --skip-email --path=%s',
			escapeshellarg( $values['BASE_URL'] ),
			escapeshellarg( $values['TITLE'] ),
			escapeshellarg( $values['ADMIN_USER'] ),
			escapeshellarg( $values['ADMIN_PASSWORD'] ),
			escapeshellarg( $values['ADMIN_EMAIL'] ),
			escapeshellarg( $wp_base_dir )
		);

		echo exec( $wp_install ) . PHP_EOL;

		// Setup config files.
		copy(
			$src_root . DIRECTORY_SEPARATOR . 'wp-config-reference.php',
			$web_root . DIRECTORY_SEPARATOR . 'wp-config.php'
		);
		rename(
			$web_root . DIRECTORY_SEPARATOR . self::WP_DIR . DIRECTORY_SEPARATOR .  'wp-config.php',
			$web_root . DIRECTORY_SEPARATOR . 'wp-config-install.php'
		);

		// Add WordPress coding standard to PHP Code Sniffer.
		$vendor_dir = $base_dir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR;
		if ( file_exists( $vendor_dir . 'bin' . DIRECTORY_SEPARATOR . 'phpcs' ) ) {
			$phpcs = sprintf(
				'%s --config-set installed_paths %s',
				$vendor_dir . 'bin' . DIRECTORY_SEPARATOR . 'phpcs',
				$vendor_dir . 'wp-coding-standards' . DIRECTORY_SEPARATOR . 'wpcs'
			);
			echo 'PHP Code Sniffer: ' . exec( $phpcs ) . PHP_EOL;
		}

		return true;
	}
}
